<?php

    $header= "C R E A T I N G";
    $h1 = "email magic";
    $h2 = "Would you like millions of new customers to enjoy your amazing food and groceries? So would we!
    It's simple: we list your menu and product lists online, help you process orders, pick them up, and deliver them to hungry pandas – in a heartbeat!
    Interested? Let's start our partnership today!";
    $pp = "Would you like millions of new customers to enjoy your amazing food and groceries? So would we!
    It's simple: we list your menu and product lists online, help you process orders, pick them up, and deliver them to hungry pandas – in a heartbeat!
    Interested? Let's start our partnership today!";
    $div1 = "Would you like millions of new customers to enjoy your amazing food and groceries? So would we!
    It's simple: we list your menu and product lists online, help you process orders, pick them up, and deliver them to hungry pandas – in a heartbeat!
    Interested? Let's start our partnership today!";
    $div2 = "Would you like millions of new customers to enjoy your amazing food and groceries? So would we!
    It's simple: we list your menu and product lists online, help you process orders, pick them up, and deliver them to hungry pandas – in a heartbeat!
    Interested? Let's start our partnership today!";
    
    $value = <<< DATA
        <style>
            .header-box{
                background-color: yellow;
                width: 500px;
                height: 300px;
            }
            .creating{
                padding-top: 120px;
                text-align: center;
            }
            .email{
                text-align: center;
            }
            .content{
                width: 380px;
                padding-left: 20px;
            }
            .mid-h2{
                font-size: 18px;
            }
            .pp{
                width: 350px
            }
            .grid{
                display: flex;
            }
            .div1{
                padding: 10px;
            }
        </style>
        <div>
            <div class="header-box">
                <P class = "creating">$header</P>
                <h1 class = "email">$h1</h1>
            </div>
            <div class="content">
                <h2 class="mid-h2">$h2</h2>
                <p class="pp">$pp</p>
            <div class="grid">
                <div class="div1">$div1</div>
                <div class="div1">$div2</div> 
            </div>
        </div>
        </div>
    DATA;
    
    echo $value;
?>